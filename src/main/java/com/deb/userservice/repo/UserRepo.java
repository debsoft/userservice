package com.deb.userservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deb.userservice.model.User;

public interface UserRepo extends JpaRepository<User, Long>{

	public User findByUserName(String username);

	public User findByEmail(String email);
}
