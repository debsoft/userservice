package com.deb.userservice.exception;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

/**
 * The Class BaseResponseDto is the base DTO. Any portable DTO can inherit the same.
 */
public class BaseErrorResponse implements Serializable{
	
	private static final long serialVersionUID = 123L;
	
	/** The http status code. */
	private HttpStatus httpStatusCode;
	
	/** The http status code value */
	private int httpStatus;
	
	/** The message. */
	private String message;
	
	/**
	 * Instantiates a new base response dto.
	 */
	public BaseErrorResponse(){
		super();
	}
	
	
	
	/**
	 * Instantiates a new base response dto.
	 *
	 * @param ok the HttpStatus
	 * @param value the value
	 * @param message the message
	 */
	public BaseErrorResponse(HttpStatus ok, int value, String message) {
		super();
		this.httpStatusCode = ok;
		this.httpStatus = value;
		this.message = message;
	}



	/**
	 * Gets the http status code.
	 *
	 * @return the http status code
	 */
	public HttpStatus getHttpStatusCode() {
		return httpStatusCode;
	}



	/**
	 * Sets the http status code.
	 *
	 * @param httpStatusCode the new http status code
	 */
	public void setHttpStatusCode(HttpStatus httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}



	/**
	 * Gets the http status.
	 *
	 * @return the http status
	 */
	public int getHttpStatus() {
		return httpStatus;
	}



	/**
	 * Sets the http status.
	 *
	 * @param httpStatus the new http status
	 */
	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}



	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}



	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}
