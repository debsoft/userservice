package com.deb.userservice.exception;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * A convenient base class for {@link ControllerAdvice @ControllerAdvice} classes
 * that wish to provide centralized exception handling across all
 * {@code @RequestMapping} methods through {@code @ExceptionHandler} methods.
 *
 * <p>This base class provides an {@code @ExceptionHandler} method for handling
 *
 * @author Debendra
 */

@RestControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler{

	
	@Autowired
	private Environment environment;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomExceptionHandler.class.getName());

	/*
	 * Handles if the required request body missed
	 * */
	@Override
	public final ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers,
				HttpStatus status, WebRequest request){
		
		String resolvedMessage =getResolvedMessgae(ex.getMessage(),"error.body.missing.msg",ex);
		LOGGER.error(resolvedMessage);
		
		
		List<String> details = new ArrayList<>();
		details.add(request.getDescription(false));
		return new ResponseEntity<>(ErrorResponse.getErrorResponse(resolvedMessage,details),HttpStatus.NOT_ACCEPTABLE);
	}

	
	/*
	 * Handles if the request mapping method mismatch
	 * */
	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
			HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		String resolvedMessage =getResolvedMessgae(ex.getMessage(),"error.request.method.notsupport.msg",ex);
		LOGGER.error(resolvedMessage);
		
		List<String> details = new ArrayList<>();
		details.add(request.getDescription(false));
		
		return new ResponseEntity<>(ErrorResponse.getErrorResponse(resolvedMessage,details),HttpStatus.METHOD_NOT_ALLOWED);
	}
	
	
	/*
	 * Handles all the exceptions if there is no specific exception handled anywhere.
	 * */
	@ExceptionHandler({Exception.class})
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request,HttpServletResponse response) {
		LOGGER.error(ex.getMessage());
		ex.printStackTrace();
		return new ResponseEntity<>(getErrorResponse(environment.getProperty("error.internal.servererror.msg"),HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	
	/**
	 * Customize the response for ResourceNotFoundException.
	 * <p>This method delegates to {@link #handleExceptionInternal}.
	 * @param resourceNotFoundException the exception
	 * @param request the current request
	 * @return a {@code ResponseEntity} instance
	 * @see ResourceNotFoundException
	 */
	@ExceptionHandler(ResourceNotFoundException.class)
	public final ResponseEntity<Object> handleResourceNotFoundException(ResourceNotFoundException resourceNotFoundException, WebRequest request) {
		String resolvedMessage =getResolvedMessgae(resourceNotFoundException.getMessage(),"error.resource.notfound.msg",resourceNotFoundException);
		LOGGER.error(resolvedMessage);
		
		return new ResponseEntity<>(getErrorResponse(resolvedMessage,HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
	}

	/**
	 * Handles the Method argument validations exception
	 * 
	 * @param meArgumentNotValidException type {@link MethodArgumentNotValidException} 
	 * @see MethodArgumentNotValidException
	 * */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException meArgumentNotValidException ,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		String resolvedMessage =getResolvedMessgae("","error.request.notvalid.msg",meArgumentNotValidException );
		LOGGER.error(resolvedMessage);
		return new ResponseEntity(getErrorResponse(resolvedMessage,HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
	}
	
	private String getResolvedMessgae(String message, String messageKey,Exception ex) {
		return ("".equals(message) || null==message)?environment.getProperty(messageKey):ex.getMessage();
	}
	
	
	
	private BaseErrorResponse getErrorResponse(String errorMessage,int httpStatus){
		BaseErrorResponse baseResponseDto = new BaseErrorResponse();
		baseResponseDto.setHttpStatus(httpStatus);
		baseResponseDto.setMessage(errorMessage);
		return baseResponseDto;
	}
	
	
}
