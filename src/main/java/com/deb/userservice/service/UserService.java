package com.deb.userservice.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.deb.userservice.dto.UserDto;
import com.deb.userservice.model.User;

@Service
public interface UserService {

	public User getUserByName(String userName);
	
	public User getUserByEmail(String email);
	
	public List<User> getAllUsers();
	
	public User create(UserDto userDto);
	
	public User update(UserDto userDto);
	
	public Boolean delete(Integer userId);
	
}
