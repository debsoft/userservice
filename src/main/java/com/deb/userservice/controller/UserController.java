package com.deb.userservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	
	@GetMapping("/user/list")
	public ResponseEntity getAllUsers() {
		return null;
	}
	
	@GetMapping("/hello")
	public ResponseEntity hello() {
		return new ResponseEntity("Hello,Welcome to the microservices world",HttpStatus.OK);
	}
}
